For GCE
```
kubectl apply -f https://gitlab.com/SIRHAMY/iamhamy/raw/master/front-iamhamy-k8s-config.yaml \
-f https://gitlab.com/SIRHAMY/iamhamy/raw/master/labs-iamhamy-k8s-config.yaml \
-f https://gitlab.com/SIRHAMY/iamhamy/raw/master/blog-iamhamy-k8s-config.yaml \
-f https://gitlab.com/SIRHAMY/iamhamy/raw/master/art-iamhamy-k8s-config.yaml \
-f https://gitlab.com/SIRHAMY/iamhamy/raw/master/nginx-ingress-k8s-config.yaml
```

Did this?
One-time setup for the ingress controller (GKE only). Check out https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/index.md for others

```
kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml


kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml
```

Get the IP and modify DNS to target
```
kubectl get ingress
```

# One-time setup for GitLab deploys

Install service account: https://docs.gitlab.com/ee/user/project/clusters/


```
kubectl get serviceaccounts
kubectl get serviceaccounts gitlab -o yaml
kubectl get secret SECRET-TOKEN-NAME -o yaml
```

# one-time setup for service accounts + deploys

* Create service acount via GUI:
    * gke service agent role
    * gke developer - may be overkill

```
gcloud components install kubectl

# probably need to do auth here

gcloud auth activate-service-account --key-file=[PATH_TO_SECRET_FILE]

gcloud container clusters get-credentials [CLUSTER_NAME] --zone [CLUSTER_LOCATION] --project [PROJECT_NAME]

kubectl apply -f [PATH_TO_CONFIG]

kubectl set image deployments/labs-iamhamy labs-iamhamy=registry.gitlab.com/sirhamy/labs-iamhamy:[SHA_TAG]

```

## Adding SSL

Set up Helm and Tiller: https://docs.helm.sh/using_helm/#installing-helm

Install cert-manager for use with LetsEncrypt: https://medium.com/oracledevs/secure-your-kubernetes-services-using-cert-manager-nginx-ingress-and-lets-encrypt-888c8b996260

```
Create the sa account, first... Requires ClusterRoleAdmin

helm init --service-account <NAME>

helm install \
  --name cert-manager \
  --namespace kube-system \
  --set ingressShim.defaultIssuerName=letsencrypt-staging \
  --set ingressShim.defaultIssuerKind=ClusterIssuer \
  stable/cert-manager
```

Create the letsencrypt-staging clusterIssuer

Modify nginx yaml to point at new clusterIssuer

Change it to production
* create production cluster issuer (pointed at url, change resource names)
* change nginx to point to it